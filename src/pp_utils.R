pp_rgb <- function(r,g,b) {
  return( r + g*256 + b*256^2)
}

pp_add_slide_title <- function(slide, text, color = 1, text_color = 1, text_size = 36, text_font = "Calibri"){
  
  
  slide_title <- slide[["Shapes"]][["Title"]]
  
  slide_title_color <- slide[["ColorScheme"]]$Colors(4)
  
  slide_title_color[["RGB"]] <- color
  slide_title$ZOrder(0) # Brint to front. Bring to back = 1
  
  slide_title_frame <- slide_title[["TextFrame"]]
  slide_title_frame[["AutoSize"]] <- 1 # ppAutoSizeShapeToFitText
  slide_title[["Top"]] <- 0
  slide_title[["Left"]] <- 0
  
  title_text <- slide_title[["TextFrame"]][["TextRange"]]
  title_text[["Text"]] <- text
  title_font <- title_text[["Font"]]
  title_font[["Color"]][["RGB"]] <- text_color
  
  title_font[["Size"]] <- text_size
  title_font[["Name"]] <- text_font
}

pp_add_gemmacon <- function(slide){
  pp_add_textbox(slide, "GEMMACON", point_a = c(970,5), point_b = c(210,30), text_size = 28, bold = FALSE, alignment = 2)
  #pp_add_shape(slide, point_a = c(999,38), point_b = c(150,25), color = 16750950, line_color = 16750950, shape = 1, transparency = 0)
  #pp_add_textbox(slide, "Anlaufmanagement", point_a = c(999,38), point_b = c(150,25), text_size = 16, color = 16777215, alignment = 2)
}

pp_add_master <- function(slide, slide_num, atl_name, subtitle = FALSE) {
  
  pp_add_textbox(slide, paste0("Projekt: ", atl_name), point_a = c(5,5), point_b = c(900,30), text_size = 36, alignment = 1)
  pp_add_gemmacon(slide)
  if (subtitle != FALSE) {
    pp_add_textbox(slide, subtitle, point_a = c(20,50), point_b = c(900,30), text_size = 26, alignment = 1)
  }
  pp_add_textbox(slide, slide_num, point_a = c(15,860), point_b = c(60,30), text_size = 16, alignment = 1)
  pp_add_textbox(slide, paste0(excel_date_string(Sys.Date()), "    ", atl_name), point_a = c(300,860), point_b = c(600,30), text_size = 16, alignment = 2)
  
}

pp_info_box <- function(slide, text, point){
  
  pp_add_shape(slide, c(point[[1]] + 15, point[[2]] + 20), c(565, 365), line_color = 3355443, shape = 1, transparency = 1)
  pp_add_shape(slide, c(point[[1]], point[[2]]), c(300, 40), 12698049, 8421504, shape = 5, adjustment = 0.25, shadow = TRUE)
  pp_add_textbox(slide, text, point_a = c(point[[1]] + 5, point[[2]] + 7), point_b = c(300, 40), text_size = 16, text_font = "Arial", bold = TRUE, alignment = 1)
}

pp_add_textbox <- function(slide, text, point_a, point_b = c(point_a[[1]]+100, point_a[[2]]+20), color = 1, text_size = 14, text_font = "Calibri", bold = FALSE, alignment = 1) {
  
  text_box <- slide[["Shapes"]]$AddTextBox(1, point_a[[1]], point_a[[2]], point_b[[1]], point_b[[2]])
  text_box_fill <- text_box[["Fill"]]
  text_box_fill[["Visible"]] <- 0
  text_box_frame <- text_box[["TextFrame"]]
  text_box_frame[["TextRange"]] <- text
  text_box_font <- text_box[["TextFrame"]][["TextRange"]][["Font"]]
  text_box_font[["Color"]][["RGB"]] <- color
  text_box_font[["Size"]] <- text_size
  text_box_font[["Name"]] <- text_font
  if (bold) {
    text_box_font[["Bold"]] <- TRUE
  }
  text_box_para <- text_box[["TextFrame"]][["TextRange"]][["ParagraphFormat"]]
  text_box_para[["Alignment"]] <- alignment
  text_font <- text_box_frame[["TextRange"]][["Font"]]
  
  
}


pp_add_shape <- function(slide, point_a, point_b, color = 1, line_color = color, transparency = 0, shape = 1, adjustment = FALSE, shadow = FALSE, shadow_type = 6, shadow_color = color, text = FALSE, text_color = 1, text_size = 14){
  bg_rect <- slide[["Shapes"]]$AddShape(shape, point_a[[1]], point_a[[2]], point_b[[1]], point_b[[2]])

  if (adjustment != FALSE) {
    adj <- bg_rect$Adjustments(1)
    adj <- adjustment
  }
  
  bg_rect_fill <- bg_rect[["Fill"]]
  bg_rect_fill[["ForeColor"]][["RGB"]] <- color
  bg_rect_fill[["Transparency"]] <- transparency
  bg_rect_line <- bg_rect[["Line"]]
  bg_rect_line[["ForeColor"]][["RGB"]] <- line_color
  
  if(text != FALSE){

     
  }
  if (shadow){
    shadow <- bg_rect[["Shadow"]]
    shadow[["Type"]] <- shadow_type
    shadow[["ForeColor"]][["RGB"]] <- shadow_color
  }

}