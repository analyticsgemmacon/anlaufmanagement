##
## $Id: excelUtils.R,v 1.1 2003/05/16 15:26:10 duncan Exp $
##

## TODO:
##   row.names for importing/exporting data.frames
##   dates/currency, general converters
##   error checking

excel_date_values <- function(){
  
  date_null <- "1899-12-30"
  today <- as.numeric(Sys.Date() - as.Date("1899-12-30"))
  
  today_string <- format(as.Date(today,date_null), "%d.%m.%Y")
  
  list(date_null = date_null, today = today, today_string = today_string)
}


excel_date_string_column <- function(column){
  
  column %>% if_else(!is.na(suppressWarnings(as.numeric(.))),excel_date_string(suppressWarnings(as.numeric(.))),.)
}

excel_date_string <- function(date){

  format(as.Date(date, "1899-12-30"), "%d.%m.%Y")
}



enc2win1252 <- function(frame){
  
  for (i in 1:ncol(frame)) {
    if (typeof(frame[,i]) == "character") {
      frame[,i] <- iconv(enc2utf8(frame[,i]),"UTF-8", "windows-1252")
    }
  }
  return(frame)
}

convert_range <- function(sheet, start = c(1,1), end) {

  return(sheet$Range(sheet$Cells(start[[1]], start[[2]]),
              sheet$Cells(end[[1]], end[[2]])))
}

convert_columns_to_excel_date <- function(frame, cols){

  frame %>% mutate_at(cols, funs(excel_date_string(.)))
}

excel_text_to_columns <- function(excel_sheet, cols, startrow, endrow, frame){
  
  map(cols, ~excel_text_to_column(excel_sheet, .x, startrow, endrow, frame %>% select(.x)))
  
}
excel_text_to_column <- function(excel_sheet, col, startrow, endrow, column){

  if (nrow(column %>% filter_at(1, all_vars(!is.na(.) & . != ""))) != 0) {
    excel_sheet$Range(excel_sheet$Cells(startrow, col),excel_sheet$Cells(endrow, col))$TextToColumns(DataType = 2)
  }

}

set_column_widths <- function(sheet, cols, widths){
  
  for (i in 1:length(cols)) {
    set_column_width(sheet,cols[i],widths[i])
  }
}

set_column_width <- function(sheet, col, l){

  column <- sheet$Columns(col)
  column[["ColumnWidth"]] <- l
  invisible()
}

create_header <- function(excel_sheet, text, row, col, size = 16, bold = TRUE) {

  create_cell(excel_sheet, row, col,
              text, -4131, "", size, TRUE, FALSE)
}

create_timestamp <- function(excel_sheet, row, col, size = 9, italtic = TRUE) {

  create_cell(excel_sheet, row, col,
              paste0("Stand: ",format(Sys.time(),"%d.%m%.%Y")),
              -4131, "", size, FALSE, TRUE)
}

create_gemmacon <- function(excel_sheet, row, col, size = 16) {

  create_cell(excel_sheet, row, col,
              "GEMMACON", -4152, "Arial", size, TRUE, FALSE)
  
}
create_cell <- function(excel_sheet, row = 1, col = 1, value, alignment, font = "", size = 16, bold = FALSE, italic = FALSE) {

  cell <- excel_sheet$Cells(row, col)
  cell[["Value"]] <- value
  cell[["HorizontalAlignment"]] <- 	alignment
  cell_font <- cell[["Font"]]
  if (font != "") {cell_font[["Name"]] <- font}
  cell_font[["Size"]] <- size
  cell_font[["Bold"]] <- bold
  cell_font[["Italic"]] <- italic
}

format_sheet <- function(sheet, font, size) {
  
  cells <- sheet$Cells()
  sheet_font <- cells[["Font"]]
  sheet_font[["Name"]] <- font
  sheet_font[["Size"]] <- size
}

format_table <- function(sheet, start = c(1,1), end, borders = TRUE, horizontal = -4108, vertical = -4108) {
  
  table_rng <- convert_range(sheet, start, end)
  
  table_rng[["HorizontalAlignment"]] <- horizontal
  table_rng[["VerticalAlignment"]] <- vertical
  table_rng[["WrapText"]] <- TRUE
  if (borders == TRUE) {
    table_borders <- table_rng[["Borders"]]
    table_borders[["LineStyle"]] <- 1
  }
}

format_rng_font <- function(sheet, start = c(1,1), end, ...) {
  
  rng <- convert_range(sheet, start, end)
  rng_font <- rng[["Font"]]
  
  args <- list(...)
  names <- names(args)
  for (i in 1:length(names)) {
    rng_font[[names[i]]] <- args[[i]]
  }
}

format_rng <- function(sheet, start = c(1,1), end, subset = "", arg = "", ...) {
  
  rng <- convert_range(sheet, start, end)
  
  if (subset != "") {
    if (arg == "") {
      rng <- rng[[subset]]
    } else {
      rng <- rng[[subset]](arg)
    }
  }
  
  args <- list(...)
  names <- names(args)
  for (i in 1:length(names)) {
    rng[[names[i]]] <- args[[i]]
  }
}
format_rng_border_around <- function(sheet, start = c(1,1), end, weight_index = 2) {
  rng <- convert_range(sheet, start, end)
  rng$BorderAround(Weight = weight_index)
}
format_rng_border_single <- function(sheet, start = c(1,1), end, border_index, line_index = 1, color_index = 0, weight_index = 2) {
  
  rng <- convert_range(sheet, start, end)
  borders <- rng$Borders(border_index)
  borders[["LineStyle"]] = line_index
  borders[["ColorIndex"]] = color_index
  borders[["Weight"]] = weight_index
}

# alignment
# xlBottom	-4107
# xlLeft	  -4131
# xlRight	  -4152
# xlTop	    -4160
# xlCenter	-4108

# border_index
# Name	           Value	Description
# xlDiagonalDown	    5	  Border running from the upper left-hand corner to the lower right of each cell in the range.
# xlDiagonalUp	      6	  Border running from the lower left-hand corner to the upper right of each cell in the range.
# xlEdgeBottom	      9	  Border at the bottom of the range.
# xlEdgeLeft	        7	  Border at the left-hand edge of the range.
# xlEdgeRight	        10	Border at the right-hand edge of the range.
# xlEdgeTop	          8	  Border at the top of the range.
# xlInsideHorizontal	12	Horizontal borders for all cells in the range except borders on the outside of the range.
# xlInsideVertical	  11	Vertical borders for all the cells in the range except borders on the outside of the range.

# weight_index:
# Name	      Value	Description
# xlHairline	1	    Hairline (thinnest border).
# xlMedium	  -4138	Medium.
# xlThick	    4	    Thick (widest border).
# xlThin	    2	    Thin.


"exportDataFrame" <-
function(df, at, withheaders = FALSE, ...)
## export the data.frame "df" into the location "at" (top,left cell)
## output the occupying range.
## TODO: row.names, more error checking
{
   nc <- dim(df)[2]
   if (nc < 1) stop("data.frame must have at least one column")
   r1 <- at$Row()                   ## 1st row in range
   c1 <- at$Column()                ## 1st col in range
   c2 <- c1 + nc - 1                ## last col (*not* num of col)
   ws <- at[["Worksheet"]]

   ## headers
   if (withheaders) {
     hdrRng <- ws$Range(ws$Cells(r1, c1), ws$Cells(r1, c2))
     hdrRng[["Value"]] <- names(df)
     rng <- ws$Cells(r1 + 1, c1) ## top cell to put 1st column
   }
   else
     rng <- ws$Cells(r1, c1)

   ## data

   for (j in seq(from = 1, to = nc)) {

       exportVector(df[,j], at = rng, ...)
       rng <- rng$Next()            ## next cell to the right
   }
   invisible(ws$Range(ws$Cells(r1, c1), ws$Cells(r1 + nrow(df), c2)))
}

"exportVector" <-
function(obj, at = NULL, byrow = FALSE, ...)
## coerce obj to a simple (no attributes) vector and export to
## the range specified at "at" (can refer to a single starting cell);
## byrow = TRUE puts obj in one row, otherwise in one column.
## How should we deal with unequal of ranges and vectors?  Currently
## we stop, modulo the special case when at refers to the starting cell.
## TODO: converters (currency, dates, etc.)
{
   n <- length(obj)
   if (n < 1) return(at)
   d <- c(at$Rows()$Count(), at$Columns()$Count())
   N <- prod(d)
   if (N == 1 && n > 1) {     ## at refers to the starting cell
      r1c1 <- c(at$Row(), at$Column())
      r2c2 <- r1c1 + if (byrow) c(0, n - 1) else c(n - 1, 0)
      ws <- at[["Worksheet"]]
      at <- ws$Range(ws$Cells(r1c1[1], r1c1[2]),
                     ws$Cells(r2c2[1], r2c2[2]))
   }
   else if (n != N)
      stop("range and length(obj) differ")

   ## currently we can only export primitives...

   if (class(obj) %in% c("logical", "integer", "numeric", "character"))
      obj <- as.vector(obj)     ## clobber attributes
   else
      obj <- as.character(obj)  ## give up -- coerce to chars

   ## here we create a C-level COM safearray
   d <- if (byrow) c(1, n) else c(n,1)
   objref <- .Call("R_create2DArray", matrix(obj, nrow = d[1], ncol = d[2]))
   at[["Value"]] <- objref
   invisible(at)
}

"importDataFrame" <-
function(rng = NULL, wks = NULL, n.guess = 5, dateFun = as.chron.excelDate)
## Create a data.frame from the range rng or from the "Used range" in
## the worksheet wks.  The excel data is assumed to be a "database" (sic)
## excel of primitive type (and possibly time/dates).
## We guess at the type of each "column" by looking at the first
## n.guess entries ... but it is only a very rough guess.
{
   if (is.null(rng) && is.null(wks))
      stop("need to specify either a range or a worksheet")
   if (is.null(rng))
      rng <- wks$UsedRange()          ## actual region
   else
      wks <- rng[["Worksheet"]]       ## need to query rng for its class
   n.areas <- rng$Areas()$Count()     ## must have only one region
   if (n.areas != 1)
      stop("data must be in a contigious block of cells")

   c1 <- rng$Column()                 ## first col
   c2 <- rng$Columns()$Count()        ## last col, provided contiguous region
   r1 <- rng$Row()                    ## first row
   r2 <- rng$Rows()$Count()           ## last row, provided contiguous region

   ## headers

   n.hdrs <- rng$ListHeaderRows()
   if (n.hdrs == 0)
      hdr <- paste("V", seq(from = 1, to = c2 - c1 + 1), sep = "")
   else if (n.hdrs == 1)
      hdr <- unlist(rng$Rows()$Item(r1)$FormulaR1C1())
   else {## collapse multi-row headers
      h <- vector("list", c2 - c1 + 1)     ## list by column
      r <- rng$Range(rng$Cells(r1, c1), rng$Cells(r1 + n.hdrs - 1, c2))
      jj <- 1
      for (j in seq(from = c1, to = c2)) {
         h[[jj]] <- unlist(r$Columns(j)$FormulaR1C1()[[1]])
         jj <- jj + 1
      }
      hdr <- sapply(h, paste, collapse = ".")
   }
   r1 <- r1 + n.hdrs

   ## Data region

   d1 <- wks$Cells(r1, c1)
   d2 <- wks$Cells(r2, c2)
   dataCols <- wks$Range(d1, d2)$Columns()
   out <- vector("list", length(hdr))
   for (j in seq(along = out)) {
      f1 <- dataCols$Item(j)
      f2 <- f1$FormulaR1C1()[[1]]
      f <- unlist(lapply(f2, function(x) if (is.null(x)) NA else x))
      cls <- guessExcelColType(f1)
      out[[j]] <- if (cls == "logical") as.logical(f) else f
   }
   names(out) <- make.names(hdr)
   as.data.frame(out)
}

"guessExcelColType" <-
function(colRng, n.guess = 5, hint = NULL)
## colRng points to an range object corresponding to one excel column
## e.g., colRng = rng$Columns()$Item("H")
## TODO: currently we return one of "logical", "numeric", "character"
## need to add "SCOMIDispatch"
{
   wf <- colRng[["Application"]][["WorksheetFunction"]]
   S.avail <- c("logical", "numeric", "integer", "character")
   ## we should get the following from the Excel type library
   fmt <- colRng[["NumberFormat"]]
   num.fmt <- c("general", "number", "currency", "accounting",
                "percentage", "fraction", "scientific")

   fld <- colRng$Rows()
   n <- fld$Count()
   k <- min(n.guess, n)
   cls <- character(k)
   c1 <- colRng$Cells(1,1)
   c2 <- colRng$Cells(k,1)

   for (i in 1:k) {
       x <- fld$Item(i)
       if (wf$IsText(x) || is.null(fmt))
          cls[i] <- "character"
       else if (wf$IsNumber(x)) {
          if (tolower(fmt) %in% num.fmt)
             cls[i] <- "numeric"
          else
             cls[i] <- "character"
          }
       else if (wf$IsLogical(x))
         cls[i] <- "logical"
       else if (wf$IsNA(x))
          cls[i] <- "NA"
       else
          cls[i] <- "character"
    }
    ## if not all rows agree, use character type
    cls <- cls[cls %in% S.avail]
    if (length(cls) == 0 || length(unique(cls)) > 1)
       return("character")
    else
       return(cls[1])
}

"as.chron.excelDate" <-
function(xlsDate, date1904 = FALSE)
{
   if (date1904) {
      orig <- c(month = 12, day = 31, year = 1903)
      off <- 0
   }
   else {
      orig <- c(month = 12, day = 31, year = 1899)
      off <- 1
   }
   chron(xlsDate - off, origin = c(month = 12, day = 31, year = 1899))
}

"as.excelData.chron" <-
function(chronDate, date1904 = FALSE)
{
   if (date1904) {
      orig <- c(month = 12, day = 31, year = 1903)
      off <- 0
   }
   else {
      orig <- c(month = 12, day = 31, year = 1899)
      off <- 1
   }
   if (any(origin(chronDate) != orig))
      origin(chronDate) <- orig
   as.numeric(chronDate) + off
}
