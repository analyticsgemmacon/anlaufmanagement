# Starting point for script

suppressPackageStartupMessages(library(openxlsx))

suppressPackageStartupMessages(library(chron))

suppressPackageStartupMessages(library(tidyverse))

suppressPackageStartupMessages(library(RDCOMClient))

suppressPackageStartupMessages(library(svglite))

cat("\n\nMNL-ATL-Reports-Listen Erstellung")
cat("\nScript-Version 3.1")

command_arguments <- commandArgs(trailingOnly = TRUE)

# project_folder <- "C:\\Users\\simon\\Documents\\Codebase\\anlaufmanagement"
# project_folder <- "C:\\Users\\SimonKraus\\Codebase\\anlaufmanagement"
# project_folder <- "C:\\Users\\smnkr\\Documents\\Code\\anlaufmanagement"
project_folder <- paste(command_arguments, sep = " ", collapse = " ")

#-------------------------------------------------------------------------------
# LOAD Util-Files and run any prerequired functions
#-------------------------------------------------------------------------------
setwd(project_folder)
source("src\\create_moving_files_utils.R", encoding = "UTF-8")
source("src\\excel_utils.R", encoding = "UTF-8")
source("src\\meta_utils.R", encoding = "UTF-8")

excel_dates <- excel_date_values()

#-------------------------------------------------------------------------------
# LOAD ALL FILES 
#-------------------------------------------------------------------------------
setwd(project_folder)
source("src\\loading_utils.R", encoding = "UTF-8")

cat("\n\nLoading Files.")
# 1. Load complete Inputfile
input_sheet <- load_input_sheet(paste0(project_folder, "\\Info\\Input.xlsx"))

# 2. Load all files into dataframes/lists
files <- load_all_files(input_sheet[[1]], project_folder)

if (length(files[[1]]) == 0) { 
  stop("\nNo recognized input files in Input Folder.")
}

special_dates <- special_dates(input_sheet[[3]])
files[[4]] <- mt_fix(files[[4]])
trimmed_atls <- trim_atls(files[[4]])
#-------------------------------------------------------------------------------
# Create ATLs
#-------------------------------------------------------------------------------
setwd(project_folder)
source("src\\atl_utils.R", encoding = "UTF-8")

cat("\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
cat("\nCreate ATLs:")
cat("\n      Remove old files:")
create_empty_atls(unlist(files[[3]], recursive = FALSE), project_folder)

# The higest level of the list can be flattened for creating the ATLs, so unlist
# with recursive = FALSE is used here to flatten.
prepared_atls <- prepare_atls(trimmed_atls, excel_dates)
cat("\n      | Logic |")

cat("\n      Excel:")
create_all_atls(unlist(files[[3]], recursive = FALSE),
                unlist(prepared_atls, recursive = FALSE), project_folder)

#-------------------------------------------------------------------------------
# Create MNL
#-------------------------------------------------------------------------------
setwd(project_folder)
source("src\\mnl_utils.R", encoding = "UTF-8")
cat("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
cat("\nCreate MNLs:")
cat("\n      Move:")
create_move_mnls(files[[1]], project_folder)

cat("\n      Logic:\n            ")
cat("|")
mnls_prepared_atl <- mnls_prepared_atl(files[[3]], files[[4]])

cat("|")
mnls_new_parts <- mnls_new_parts(mnls_prepared_atl, files[[5]])

prepared_mnls_old <- prepare_mnls_old(files[[5]])

cat("|")
mnls_updated_parts <- mnls_updated_parts(mnls_prepared_atl,
                                         prepared_mnls_old,
                                         mnls_new_parts)
cat("|")

cat("\n      Excel:")
create_all_mnls(files[[1]], mnls_new_parts, mnls_updated_parts, project_folder)

#-------------------------------------------------------------------------------
# Listen
#-------------------------------------------------------------------------------
setwd(project_folder)
source("src\\listen_utils.R", encoding = "UTF-8")

cat("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
cat("\nCreate Listen:\n")

cat("\n      Remove old files:")

create_move_lists(files[[1]], project_folder)

anflug_dates <- anflug_dates()

cluster_names <- create_cluster_names(anflug_dates)

prepared_tables <- prepare_tabels(trimmed_atls, special_dates, anflug_dates, input_sheet[[2]])
cat("\n      | Logic |")
cat("\n      Excel:")
create_lists_file(files[[1]], files[[3]], select_prepared_tables(prepared_tables), project_folder, cluster_names)

#-------------------------------------------------------------------------------
# Reports
#-------------------------------------------------------------------------------

setwd(project_folder)
source("src\\reports_utils.R", encoding = "UTF-8")

cat("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
cat("\nCreate Reports:\n")
files[[6]] <- create_all_empty_reports(files[[1]], project_folder)
cat("      | Logic |")
cat("\n      Excel:")
create_all_reports_excel(files[[1]], files[[6]], mnls_updated_parts, files[[3]], files[[4]])

#-------------------------------------------------------------------------------
# End
#-------------------------------------------------------------------------------
cat("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
cat("\nScript-Ende\n")
